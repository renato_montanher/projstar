﻿using UnityEngine;
using System.Collections;

public class MeteorTypeABehavior : MonoBehaviour {

    private Rigidbody2D rb;
    private Vector3 direction;
    private Vector3 playerPosition;

    [SerializeField]
    private float speed;

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody2D>();

        speed = 10.0f;

        // Recupera a posição atual do player

        GameObject player = GameObject.FindGameObjectWithTag("player") as GameObject;

        playerPosition = player.transform.position;

    }
	
	// Update is called once per frame
	void Update () {

        Moviment();

	}

    void Moviment()
    {

        Vector2 direction = (playerPosition - transform.position);

        //angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        //rb.MoveRotation(angle);

        //direction = -transform.up * speed;

        rb.AddForce(direction);

    }
}
