﻿using UnityEngine;
using System.Collections;

public class TouchFollow : MonoBehaviour {

	Rigidbody2D rb;
	Vector3 t;
	public float speed = 0.1F;
	//private SpringJoint2D springJoint;
	float angle;


	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody2D>();
		t.z = 0;
		//myGameObject = this.gameObject;
		//springJoint = myGameObject.AddComponent<SpringJoint2D>();
	}
			
	void FixedUpdate() {

        Moviment();

	}
	

    void Moviment()
    {

        t = Input.mousePosition;
        t = Camera.main.ScreenToWorldPoint(t);

        //angle = Mathf.Atan2(transform.position.y-t.y, transform.position.x-t.x)*180 / Mathf.PI;
        Vector2 direction = (t - transform.position).normalized;
        //angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        //rb.MoveRotation(angle);
        transform.up = direction;

        rb.AddForce(direction * speed);

    }


}
